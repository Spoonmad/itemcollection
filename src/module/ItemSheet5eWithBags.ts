//@ts-ignore
import ItemSheet5e from "../../../systems/dnd5e/module/item/sheet.js";
import CONSTANTS from "./constants.js";
import { error, info, log, warn } from "./libs/lib.js";

// let knownSheets = {};
// let templates = {};
let canAlwaysAddToBag;
let canAlwaysAddToBagTypes;

Hooks.once("ready", () => {
  if (game.i18n.localize(CONSTANTS.MODULE_NAME+".canAlwaysAddToBagv9") !== CONSTANTS.MODULE_NAME+".canAlwaysAddToBagv9") {
    canAlwaysAddToBag = game.i18n.localize(CONSTANTS.MODULE_NAME+".canAlwaysAddToBagv9").split(",").map(s=>s.trim());
    canAlwaysAddToBagTypes = game.i18n.localize(CONSTANTS.MODULE_NAME+".canAlwaysAddToBagTypesv9").split("'").map(s=>s.trim());
  
  } else {
  canAlwaysAddToBag = game.i18n.localize(CONSTANTS.MODULE_NAME+".canAlwaysAddToBag");
  canAlwaysAddToBagTypes = game.i18n.localize(CONSTANTS.MODULE_NAME+".canAlwaysAddToBagTypes");
  }
});

export class ItemSheet5eWithBags extends ItemSheet5e {
  [x: string]: any;

  item: any;
  // get item(): any {return super.object}
  options: any;
  constructor(...args) {
    super(...args);
    this.options.width = 570;
    this.options.height = 500;
    // this.item = args[0];
  }

  /** @override */  
  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      width: 570,
      height: 500,
      //@ts-ignore
      showUnpreparedSpells: true,
      tabs: [{navSelector: ".tabs", contentSelector: ".sheet-body", initial: "details"}]

    });
    return options;
  }

  /** @override */
  get template() {
    return `/modules/${CONSTANTS.MODULE_NAME}/templates/bag-sheet.html`;
  }

  //@ts-ignore
  render(...args) {
    super.render(...args);
  }
  //@ts-ignore
  async _onSubmit(event, {updateData=null, preventClose=false}={}) {
      super._onSubmit(event, {updateData, preventClose})
  }
  blankCurrency = {pp: 0, gp: 0, ep: 0, sp: 0, cp: 0};

  /** @override */
  async getData(options) {
    const type = this.item.type;

    if (!["backpack"].includes(type)) {
      error(game.i18n.localize(CONSTANTS.MODULE_NAME+".wrongType"), true);
      this.options.editable = false;
      options.editable = false;
      return super.getData(options);
    };

    const item = this.item;
    const data:any = await super.getData(options);
    data.flags = duplicate(item.data.flags);
    // setProperty(data.flags.itemcollection, "contentsData", await this.item.getFlag("itemcollection", "contentsData"));

    //@ts-ignore
    data.currencies = Object.entries(CONFIG.DND5E.currencies).reduce((obj, [k, c]) => {
      //@ts-ignore
      obj[k] = c.label;
      return obj;
    }, {});


    if (!hasProperty(data.flags, CONSTANTS.MODULE_NAME+".bagWeight"))
      setProperty(data.flags, CONSTANTS.MODULE_NAME+".bagWeight", 0);
    if (!hasProperty(data.flags, CONSTANTS.MODULE_NAME+".goldValue"))
      setProperty(data.flags,CONSTANTS.MODULE_NAME+".goldValue",  0);
    if (!hasProperty(data.flags, CONSTANTS.MODULE_NAME+".contentsData"))
      setProperty(data.flags,CONSTANTS.MODULE_NAME+".contentsData", []);
    if (!hasProperty(data.flags, CONSTANTS.MODULE_NAME+".importSpells"))
      setProperty(data.flags,CONSTANTS.MODULE_NAME+".importSpells", false);
    
    //this.baseapps.options.editable = this.baseapps.options.editable// && (!this.item.actor || !this.item.actor.token);
    data.hasDetails = true;
    if (game.settings.get(CONSTANTS.MODULE_NAME, "sortBagContents")) {
      data.flags.itemcollection.contentsData.sort((a,b) => {
        if (a.type === "spell" && b.type !== "spell") return 1;
        if (a.type !== "spell" && b.type === "spell") return -1;
        // if (a.type !== b.type) return (a.type < b.type ? -1 : 1);
        if (a.type !== "spell") return (a.name < b.name ? -1 : 1);
        if (a.data.level !== b.data.level) return (a.data.level - b.data.level);
        return a.name < b.name ? -1 : 1;
      });
    }
    data.isGM = game.user?.isGM;
    //TODO check this out
    for (const i of data.flags.itemcollection.contentsData){
      i.isBackpack = i.type === "backpack"
      i.isSpell = i.type === "spell";
      i.totalWeight = i.data.weight * i.data.quantity;
    }
    data.canImportExport = item.parent !== undefined;
    data.isOwned = item.parent !== undefined;
    data.canConvertToGold = game.settings.get(CONSTANTS.MODULE_NAME, 'goldConversion');
    data.totalGoldValue = this.item.calcPrice();
    data.itemsWeight = this.item.calcItemWeight();
    data.weight = this.item.calcWeight();
    let parent = this.item.parent;
    data.parentName = "";
    while (parent) {
      data.parentName += `<- ${parent.name} `
      parent = parent.parent;
    }
    if (data.parentName.length > 0) data.parentName = `(${data.parentName})`;

    data.weightUnit = game.settings.get("dnd5e", "metricWeightUnits")
          ? game.i18n.localize("DND5E.AbbreviationKgs")
          : game.i18n.localize("DND5E.AbbreviationLbs");

    return data;
  }

  async _onDragItemStart(event) {
    event.stopPropagation();
    const items = this.item.getFlag(CONSTANTS.MODULE_NAME, "contents");
    const itemId = event.currentTarget.dataset.itemId;
    const item = this.item.items.get(itemId);
    event.dataTransfer.setData("text/plain", JSON.stringify({
      type: "Item",
      data: item
    }));
    await this.item.deleteEmbeddedDocuments("Item", [itemId]);
    //this.render(false);
  }

  canAdd(itemData) {
    // Check that the item is not too heavy for the bag.
    const bagCapacity = this.item.data.data.capacity.value;
    if (bagCapacity === 0) return true;
    if (canAlwaysAddToBagTypes.some(name=>itemData.name.includes(name))) return true;
    if (canAlwaysAddToBag.includes(itemData.name)) return true;

    const itemQuantity = itemData.data.quantity || 1;
    if (this.item.data.data.capacity.type === "items") {
      const itemCount = this.item.containedItemCount()
      return itemCount + itemQuantity <= bagCapacity;
    }
    const newWeight = this.item.calcItemWeight({ignoreItems: canAlwaysAddToBag, ignoreTypes: canAlwaysAddToBagTypes  }) + (itemData.data.weight ?? 0) * itemQuantity;
    return bagCapacity >= newWeight;
  }

  async _onDrop(event) {
    event.preventDefault();
    event.stopPropagation();
    let data;
    try {
      data = JSON.parse(event.dataTransfer.getData('text/plain'));
      if ( data.type !== "Item" ) {
        log(`Bags only accept items`);
        return false;
      }
    }
    catch (err) {
      log(`drop error`)
      log(event.dataTransfer.getData('text/plain'));
      log(err);
      return false;
    }
    // Case 1 - Data explicitly provided
    let actor = game.actors?.get(data.actorId);
    
    if (data.tokenId) {
      const uuid = `Scene.${data.sceneId}.Token.${data.tokenId}`;
      const tokenDocument = await fromUuid(uuid);
      //@ts-ignore .actor
      if (tokenDocument) actor = tokenDocument.actor;
    }
    if ( data.data ) {
      // Check up the chain that we are not dropping one of our parents onto us.
      let canAdd = this.item.id !== data.data._id;
      let parent = this.item.parent;
      let count = 0;
      while (parent && count < 10) { // Don't allow drops of anything in the parent chain or the item will disappear.
        count += 1;
        //@ts-ignore parent.id
        canAdd = canAdd && (parent.id !== data.data._id);;
        parent = parent.parent;
      }
      if (!canAdd) {
        log(`Cant drop on yourself`);
        info(game.i18n.localize('itemcollection.ExtradimensionalVortex'), true);
        throw error("Dragging bag onto istelf or ancestor opens a planar vortex and you are sucked into it")
      }
      // drop from player characters or another bag.
      if (this.canAdd(data.data)) {
          // will fit in the bag so add it to the bag and delete from the owning actor if there is one.
          const toDelete = data.data._id;
          await this.item.createEmbeddedDocuments("Item", [data.data]);
          //@ts-ignore deleteEmbeddedDocuments
          if (actor && (actor.data.type === "character" || actor.isToken)) await actor.deleteEmbeddedDocuments("Item", [toDelete]);
          return false;
      }
      // Item will not fit in the bag what to do?
      else if (this.item.parent) { // this bag is owned by an actor - drop into the inventory instead.
          //@ts-ignore
          if (actor && actor.data.type === "character") await actor.deleteEmbeddedDocuments("Item", [data.data._id]);
          await this.item.parent.createEmbeddedDocuments("Item", [data.data]);
          info(game.i18n.localize('itemcollection.AlternateDropInInventory'), true);
          return false;
      }
      // Last resort accept the drop anyway so that the item wont disappear.
      else if (!actor) await this.item.createEmbeddedDocuments("Item", [data.data]); 
    }

    // Case 2 - Import from a Compendium pack
    else if ( data.pack ) {
      this._importItemFromCollection(data.pack, data.id);
    }

    // Case 3 - Import from World entity
    else {
      const item = <Item>game.items?.get(data.id);
      if (this.canAdd(item.data)) { // item will fit in the bag
        //@ts-ignore toJSON
        const itemData = item.data.toJSON();
        await this.item.createEmbeddedDocuments("Item", [itemData]);
      } else {
        log(`no room in bag for dropped item`);
        info(game.i18n.localize('itemcollection.NoRoomInBag'),true);
      }
    }
    return false;
  }

  async _importItemFromCollection(collection, entryId) {
    //@ts-ignore
    const item = await game.packs.get(collection).getDocument(entryId);
    if (!item) return;
    //@ts-ignore toJSON
    return this.item.createEmbeddedDocuments("Item", item.data.toJSON())
  }


  async _itemExport(event) {
    const li = $(event.currentTarget).parents(".item");
    const id = li.attr("data-item-id");
    if (!this.item.parent) return;
    const item = this.item.items.get(id);
    if (!item) {
      console.error(`Item ${id} not found`)
    }
    Hooks.once("updateItem", () => {
      this.item.parent.createEmbeddedDocuments("Item", [item.data.toJSON()])
    });
    await this.item.deleteEmbeddedDocuments("Item", [item.id])
    this.render();
  }

  async _itemSplit(event) {
    const li = $(event.currentTarget).parents(".item");
    const id = li.attr("data-item-id");
    const item = await this.item.getEmbeddedDocument("Item", id);
    if (item.type === "backpack") {
      warn(`can't split a bag`, true);
      return; //can't split a bag
    }
    if (item.data.data.quantity < 2) {
      warn(`not enough to split (quantity msut be > 1)`, true);
      return; // not enough to split
    }
    const itemData = item.data.toJSON();
    const newQuantity = Math.floor(item.data.data.quantity / 2);
    // item.data.data.quantity -= newQuantity;
    itemData.data.quantity = newQuantity;
    Hooks.once("updateItem", () => this.item.createEmbeddedDocuments("Item", [itemData]));
    await item.update({"data.quantity": item.data.data.quantity - newQuantity})
  }

  async _itemConvertToGold(event) {
    if (!game.settings.get(CONSTANTS.MODULE_NAME, 'goldConversion')) return;
    const li = $(event.currentTarget).parents(".item");
    const id = li.attr("data-item-id");
    const item = await this.item.getEmbeddedDocument("Item", id);
    if (!item) {
      return; // should not happen
    }
    const goldValue = item.calcPrice();
    if (goldValue <= 0){
      return;
    }
    const currency = duplicate(this.item.data.data.currency);
    currency.gp = currency.gp + Math.round((goldValue * <number>game.settings.get(CONSTANTS.MODULE_NAME, 'goldConversionPercentage') / 100) * 100) / 100;
    Hooks.once("updateItem", () => this.item.update({"data.currency": currency}))
    // remove the item
    await item.delete();
    return 
  }

  // since diffObject balks on arrays need to do a deeper compare
  object_equals( x, y ) {
    if ( x === y ) {
      return true;
    }
    // if both x and y are null or undefined and exactly the same
    if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) {
      return false;
    }
    // if they are not strictly equal, they both need to be Objects
    if ( x.type !== y.type ) {
      return false;
    }
    for (const p in x ) {
      if (p === "quantity") {
        continue; 
      }
      // ignore quantity
      if ( ! Object.prototype.hasOwnProperty.call(x, p) ) {
        continue;
      }
      // other properties were tested using x.constructor === y.constructor
      if ( ! Object.prototype.hasOwnProperty.call(y, p ) ) {
        return false;
      }
      // allows to compare x[ p ] and y[ p ] when set to undefined
      if ( x[ p ] === y[ p ] ) {
        continue;
      }
      // if they have the same strict value or identity then they are equal
      if ( typeof( x[ p ] ) !== "object" ) {
        return false;
      }
      // Numbers, Strings, Functions, Booleans must be strictly equal
      if ( ! this.object_equals( x[ p ],  y[ p ] ) ) {
        return false;
      }
      // Objects and Arrays must be tested recursively
    }

    for (const p in y ){
      if ( Object.prototype.hasOwnProperty.call(y, p) && ! Object.prototype.hasOwnProperty.call(x, p) ){
        return false;
        // allows x[ p ] to be set to undefined
      }
    }
    return true;
  }

  async _compactAll() {
    const items = duplicate(this.item.getFlag("itemcollection", "contentsData"));
    const mergedItems = {};
    const keptItems:Item[] = [];
    for (const itemData of items) {
      if (!itemData.flags.itemcollection?.contentsData) {
        let canMerge = false;
        if (mergedItems[itemData.name]) {
          // let diffs = Object.keys(diffObject(mergedItems[itemData.name].data, itemData.data));
          // canMerge = (diffs.length === 0) || (diffs.length === 1 && diffs[0] === "quantity")
          // TODO consideer if we need to include flags & effects in the compare.
          canMerge = this.object_equals(mergedItems[itemData.name].data,itemData.data);
        };
        if (mergedItems[itemData.name] && canMerge) {
          const oldQ = parseInt(mergedItems[itemData.name].data.quantity);
          const increment = parseInt(itemData.data.quantity || 1);
          if (mergedItems[itemData.name].data.quantity) mergedItems[itemData.name].data.quantity = oldQ + increment;
        } else if (mergedItems[itemData.name]) { // we would like to merge but can't
          keptItems.push(itemData);
        } else {
          mergedItems[itemData.name] = itemData;
        }
      } else keptItems.push(itemData);
    }
    const newItems = Object.values(mergedItems).concat(keptItems);
    const toDelete = items.map(i=>i._id)
    Hooks.once("updateItem", () => {
      this.item.deleteEmbeddedDocuments("Item", toDelete);
    });
    return this.item.createEmbeddedDocuments("Item", newItems)
  }

  async _exportAll(event) {
    if (!isNewerVersion(game.version, "0.8.9")) {
      warn("Disabled due to bugs - use drag and drop or single item export", true);
      return;
    }
    if (!this.item.parent) return;
    if (this.item.items.length === 0) return;
    const itemsData = duplicate(getProperty(this.item.data.flags, "itemcollection.contentsData") ?? []);
    const toDelete = itemsData.map(idata => idata._id);
    await this.item.parent.createEmbeddedDocuments("Item", itemsData);
    await this.updateParentCurrency(this.item.data.data.currency);
    await this.item.deleteEmbeddedDocuments("Item", toDelete)
    // this.render(true);
  }
  
  getParentCurrency() {
    if (!this.item.parent) return;
    return this.item.parent.data.data.currency;
  }

  async setParentCurrency(currency) {
    if (!this.item.parent) return;
    this.item.parent.update({"data.currency": currency});
  }
  async updateParentCurrency(addedCurrency) {
    const existingCurrency = this.getParentCurrency();
    // TODO add the currencies together
    const newCurrency = duplicate(this.blankCurrency);

    for (const key of Object.keys(this.blankCurrency)) {
      newCurrency[key] = (addedCurrency[key] ?? 0) + (existingCurrency[key] ?? 0);
    }
    Hooks.once("updateItem", () => {
      this.setParentCurrency(newCurrency);
    })
    await this.item.update({"data.currency": this.blankCurrency})
  }
  
  async _editItem(ev) {
    ev.preventDefault();
    const li = $(ev.currentTarget).parents(".item");
    const id = li.attr("data-item-id");
    const item = <Item>this.item.items.get(id);
    if (!item){
      throw error(`Item ${id} not found in Bag ${this.item._id}`, true);
    }
    // let item = this.items[idx];
    return item.sheet?.render(true);
  }

  _onItemSummary(event) {
    // return;
    event.preventDefault();
    const li = $(event.currentTarget).parents(".item"),
        item = this.item.items.get(li.data("item-id")),
        chatData = item.getChatData({secrets: game.user?.isGM});

    // Toggle summary
    if ( li.hasClass("expanded") ) {
      const summary = li.children(".item-summary");
      summary.slideUp(200, () => summary.remove());
    } else {
      const div = $(`<div class="item-summary">${chatData.description.value}</div>`);
      const props = $(`<div class="item-properties"></div>`);
      chatData.properties.forEach(p => props.append(`<span class="tag">${p}</span>`));
      div.append(props);
      li.append(div.hide());
      div.slideDown(200);
    }
    li.toggleClass("expanded");
  }

  activateListeners(html) {
    super.activateListeners(html);

    // Everything below is only needed if the sheet is editable
    if ( !this.options.editable ) return;
    // Make the Actor sheet droppable for Items if it is not owned by a token or npc
    if (this.item.type === "backpack") {  /*|| this.item.type === "loot"*/
      //@ts-ignore TODO fix this
        this.form.ondragover = ev => this._onDragOver(ev);
      //@ts-ignore
      this.form.ondrop = ev => this._onDrop(ev);

        html.find('.item').each((i, li) => {
          li.setAttribute("draggable", true);
          li.addEventListener("dragstart", this._onDragItemStart.bind(this), false);
        });

        document.addEventListener("dragend", this._onDragEnd.bind(this));
        // html[0].ondragend = this._onDragEnd.bind(this);
        html.find('.item .item-name.rollable h4').click(event => this._onItemSummary(event));
    }

    html.find("input").focusout(this._onUnfocus.bind(this));

      // Delete Inventory Item
    html.find('.item-delete').click(async ev => {
      const li = $(ev.currentTarget).parents(".item"),
      itemId = li.attr("data-item-id");
      await this.item.deleteEmbeddedDocuments("Item", [itemId]);
      this.render();
    });

    html.find('.item-edit').click(ev => this._editItem(ev));
    html.find('.item-export-all').click(ev => this._exportAll(event));
    html.find('.item-export').click(ev => this._itemExport(ev));
    html.find('.item-compact-all').click(ev => this._compactAll());
    html.find('.item-import-all').click(ev => this._importAllItemsFromParent(this.item.parent));
    html.find('.item-split').click(ev => this._itemSplit(ev));
    html.find('.item-convertToGold').click(ev => this._itemConvertToGold(ev));
    html.find('.item .item-name h4').click(event => this._onItemSummary(event));
  //  html.find('.bag-equipped').click(ev => this._toggleEquipped(ev));
  }

  async _importAllItemsFromParent(parent) {
    if (!isNewerVersion(game.version, "0.8.9")) {
      warn("Disabled due to bugs - use drag and drop", true);
      return;
    }
    if (!parent) return;
    const itemsToImport:Item[] = [];
    for (const testItem of parent.items) {
      if (["weapon", "equipment", "consumable", "tool", "loot", "spell"].includes(testItem.type))
        itemsToImport.push(testItem.data.toJSON());
    }
    const itemsToDelete = itemsToImport.map(itemData => itemData.id);
    await this.item.createEmbeddedDocuments("Item", itemsToImport);
    await parent.deleteEmbeddedDocuments("Item", itemsToDelete);

    this.render();
  }

  _onDragEnd(event) {
    event.preventDefault();
    return false;
  }
  _onDragOver(event) {
    event.preventDefault();
    return false;
  }

  _onUnfocus(event) {
    //@ts-ignore
    this._submitting = true;
    setTimeout(() => {
      const hasFocus = $(":focus").length;
      if ( !hasFocus ) {
        this._onSubmit(event);
      }
      //@ts-ignore
      this._submitting = false;
    }, 25);
  }
}
